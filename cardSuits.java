/* 
 *  Developed By	 : Sreenath T V
 *  Student ID		 : 646823
 *  File Description : This file implements cardSuits enumerator which 
 *  represents the suits of cards.
 */

// Represents the suits of cards 
public enum cardSuits {
	C, D, H, S;

	// To parse or validate the inputed suits
	static cardSuits parseSuits(String inputSuit) {
		for (cardSuits suit : cardSuits.values()) {
			if (inputSuit.equalsIgnoreCase(suit.toString())) {
				return suit;
			}
		}
		return null;
	}
}