/* 
 *  Developed By	 : Sreenath T V
 *  Student ID		 : 646823
 *  File Description : This file implements the player class which encapsulates
 *  its instance variables and methods which are used for a wide range of 
 *  activities from classifying its hands to deciding winner.
 */

/*
 * This class uses an array of card class objects 'cardsInHand' to store 5 
 * cards of a hand. Also, it uses 'winnerDecisionArray' to store the 
 * winning decision factor in each class. The string 'handDescription' stores
 * the description of the hand to be displayed on the output  
 */

import java.util.Arrays;

public class player implements Comparable<player> {
	private int player_number;
	private card[] cardsInHand = new card[5];
	private handClasses pokerHandClass;
	private String handDescription;
	private cardRanks[] winnerDecisionArray;

	enum handClasses {
		HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE_OF_A_KIND, STRAIGHT, FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH
	};

	// Constructor
	public player(int player_number) {
		this.player_number = player_number + 1;
		pokerHandClass = handClasses.HIGH_CARD;
	}

	// Copy Constructor
	public player(player object1) {
		player_number = object1.player_number;
		for (int i = 0; i < object1.cardsInHand.length; i++) {
			cardsInHand[i] = object1.cardsInHand[i];
		}
		pokerHandClass = object1.pokerHandClass;
		handDescription = object1.handDescription;
		winnerDecisionArray = new cardRanks[object1.winnerDecisionArray.length];
		for (int i = 0; i < object1.winnerDecisionArray.length; i++) {
			winnerDecisionArray[i] = object1.winnerDecisionArray[i];
		}
	}

	// Inserting cards into player's hand
	public void addCard(card getCard, int i) {
		cardsInHand[i] = getCard;
	}

	// Describing the hand based on its hand class
	private void addDescription() {
		switch (pokerHandClass) {
		case HIGH_CARD:
			handDescription = winnerDecisionArray[0].toValue() + "-high";
			break;
		case ONE_PAIR:
			handDescription = "Pair of " + winnerDecisionArray[0].toValue()
					+ "s";
			break;
		case TWO_PAIR:
			handDescription = winnerDecisionArray[0].toValue() + "s over "
					+ winnerDecisionArray[1].toValue() + "s";
			break;
		case THREE_OF_A_KIND:
			handDescription = "Three " + winnerDecisionArray[0].toValue() + "s";
			break;
		case STRAIGHT:
			handDescription = winnerDecisionArray[0].toValue()
					+ "-high straight";
			break;
		case FLUSH:
			handDescription = winnerDecisionArray[0].toValue() + "-high flush";
			break;
		case FULL_HOUSE:
			handDescription = winnerDecisionArray[0].toValue() + "s full of "
					+ winnerDecisionArray[1].toValue() + "s";
			break;
		case FOUR_OF_A_KIND:
			handDescription = "Four " + winnerDecisionArray[0].toValue() + "s";
			break;
		case STRAIGHT_FLUSH:
			handDescription = winnerDecisionArray[0].toValue()
					+ "-high straight flush";
			break;
		}
	}

	// Getter method for instance variable playerNumber
	public int getPlayerNumber() {
		return player_number;
	}

	// Compares winnerDecisionArray when two players belongs to same hand class
	public player decideWinner(player tempPlayer) {
		for (int i = 0; i < winnerDecisionArray.length; i++) {
			int tempValue = 0;
			tempValue = this.winnerDecisionArray[i]
					.compareTo(tempPlayer.winnerDecisionArray[i]);
			if (tempValue > 0) {
				return this;
			} else if (tempValue < 0) {
				return tempPlayer;
			} else {
				continue;
			}
		}
		return null;
	}

	// Displays the hand description
	public void display() {
		System.out.println("Player " + player_number + ": " + handDescription);
	}

	// Used for comparing class objects
	public int compareTo(player tempPlayer) {
		return this.pokerHandClass.compareTo(tempPlayer.pokerHandClass);
	}

	// Checks whether two class object's hands belongs to same class
	public boolean equals(player tempPlayer) {
		return this.pokerHandClass.equals(tempPlayer.pokerHandClass);
	}

	/*
	 * The next four methods are used for classifying the player's hand
	 */

	// Classifies players hand
	public void classfyHand() {
		boolean isStraight = false, isFlush = false;
		handClasses tempClass;

		cardRanks[] rank = new cardRanks[5];
		cardSuits[] suit = new cardSuits[5];
		for (int i = 0; i < cardsInHand.length; i++) {
			rank[i] = cardsInHand[i].getRank();
			suit[i] = cardsInHand[i].getSuit();
		}

		isStraight = checkStraight(rank);
		isFlush = checkFlush(suit);

		if (isStraight && isFlush) {
			pokerHandClass = handClasses.STRAIGHT_FLUSH;
		} else if (isFlush) {
			pokerHandClass = handClasses.FLUSH;
			Arrays.sort(rank);
			winnerDecisionArray = new cardRanks[rank.length];
			for (int i = rank.length - 1; i >= 0; i--) {
				winnerDecisionArray[i] = rank[i];
			}
		} else if (isStraight) {
			pokerHandClass = handClasses.STRAIGHT;
		}
		tempClass = FindNOfAKind(rank);

		/*
		 * If a hand belongs to more than one classification, then finding out
		 * the highest class to which the hand belongs.
		 */

		if (pokerHandClass.compareTo(tempClass) < 0) {
			pokerHandClass = tempClass;
		}

		addDescription();
	}

	// To check whether the hand is Straight
	public boolean checkStraight(cardRanks[] ranks) {
		Arrays.sort(ranks);
		for (int i = 0; i < ranks.length - 1; i++) {
			if (ranks[i + 1].ordinal() - ranks[i].ordinal() != 1) {
				return false;
			}
		}
		winnerDecisionArray = new cardRanks[ranks.length];
		for (int i = ranks.length - 1; i >= 0; i--) {
			winnerDecisionArray[i] = ranks[i];
		}
		return true;
	}

	// To check whether the hand belongs to Flush
	public boolean checkFlush(cardSuits[] suits) {
		cardSuits tempSuit;
		tempSuit = suits[0];
		for (cardSuits iterateSuits : suits) {
			if (iterateSuits != tempSuit) {
				return false;
			}
		}
		return true;
	}

	/*
	 * Logic Used : First sort the array. Then calculate the number of distinct
	 * ranks in the array. If there are 5 distinct elements, it means its a HIGH
	 * card. Similarly, if there are 2 distinct elements, it could be either
	 * FULLHOUSE or a FOUROFAKIND. Similarly checking for other cases
	 */

	// To check whether the hand belongs to 'n of a kind' or pair categories
	public handClasses FindNOfAKind(cardRanks[] ranks) {
		cardRanks[] tempRanks = new cardRanks[5];
		handClasses returnValue = handClasses.HIGH_CARD;
		int[] tempCount = new int[5];
		int j = 0, validLength = 0, firstIndex = 0, secondIndex = 1, thirdIndex = 2;

		Arrays.sort(ranks);

		tempRanks[j] = ranks[ranks.length - 1];
		tempCount[j] = 1;
		validLength++;
		for (int i = ranks.length - 2; i >= 0; i--) {
			if (tempRanks[j] == ranks[i]) {
				tempCount[j] += 1;
			} else {
				tempRanks[++j] = ranks[i];
				tempCount[j] = 1;
				validLength++;
			}
		}

		winnerDecisionArray = new cardRanks[validLength];
		switch (validLength) {
		case 2:
			for (int i = 0; i < validLength; i++) {
				if (tempCount[i] == 1 || tempCount[i] == 2) {
					winnerDecisionArray[secondIndex] = tempRanks[i];
				} else if (tempCount[i] == 4) {
					winnerDecisionArray[firstIndex] = tempRanks[i];
					returnValue = handClasses.FOUR_OF_A_KIND;
				} else {
					winnerDecisionArray[firstIndex] = tempRanks[i];
					returnValue = handClasses.FULL_HOUSE;
				}
			}
			break;
		case 3:
			for (int i = 0; i < validLength; i++) {
				if (tempCount[i] == 2) {
					if (winnerDecisionArray[firstIndex] == null) {
						winnerDecisionArray[firstIndex] = tempRanks[i];
					} else if (winnerDecisionArray[secondIndex] == null) {
						winnerDecisionArray[secondIndex] = tempRanks[i];
					} else {
						winnerDecisionArray[thirdIndex] = winnerDecisionArray[1];
						winnerDecisionArray[secondIndex] = tempRanks[i];
					}
					returnValue = handClasses.TWO_PAIR;
				} else if (tempCount[i] == 3) {
					winnerDecisionArray[firstIndex] = ranks[i];
					returnValue = handClasses.THREE_OF_A_KIND;
				} else {
					if (winnerDecisionArray[secondIndex] == null) {
						winnerDecisionArray[secondIndex] = tempRanks[i];
					} else {
						winnerDecisionArray[thirdIndex] = tempRanks[i];
					}
				}
			}
			break;
		case 4:
			for (int i = 0, k = 1; i < validLength; i++) {
				if (tempCount[i] == 2) {
					winnerDecisionArray[firstIndex] = tempRanks[i];
				} else {
					winnerDecisionArray[k] = tempRanks[i];
					k++;
				}
			}
			returnValue = handClasses.ONE_PAIR;
			break;
		default:
			for (int i = 0; i < validLength; i++) {
				winnerDecisionArray[i] = tempRanks[i];
			}
			returnValue = handClasses.HIGH_CARD;
			break;
		}

		return returnValue;
	}

}