/* 
 *  Developed By	 : Sreenath T V
 *  Student ID		 : 646823
 *  File Description : This file has the implementation of card class.
 */

// Represents a card
public class card {
	private cardRanks rank;
	private cardSuits suit;

	// Default Constructor
	public card() {
		rank = null;
		suit = null;
	}

	// Constructor with arguments
	public card(cardRanks rank, cardSuits suit) {
		this.rank = rank;
		this.suit = suit;
	}

	// To get rank of the card
	public cardRanks getRank() {
		return rank;
	}

	// To get suit of the card
	public cardSuits getSuit() {
		return suit;
	}

}