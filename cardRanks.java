/* 
 *  Developed By	 : Sreenath T V
 *  Student ID		 : 646823
 *  File Description : This file implements cardRanks enumerator which 
 *  represents the ranks of cards.
 */

// Represents the ranks of cards 
public enum cardRanks {
	TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE;

	// To parse or validate the inputed ranks
	static cardRanks parseRanks(String inputRank) {
		cardRanks[] tempRank = cardRanks.values();
		try {
			int rank = Integer.parseInt(inputRank);
			if (rank > 1 && rank < 10) {
				return tempRank[rank - 2];
			}
		} catch (NumberFormatException e) {
			// Check if the given card belongs to Ace, King, Queen, Jack or ten
			for (int i = 8; i < tempRank.length; i++) {
				if (inputRank.equalsIgnoreCase(tempRank[i].toString()
						.substring(0, 1))) {
					return tempRank[i];
				}
			}
		}
		return null;
	}

	// Formatting for displaying hand description
	public String toValue() {
		switch (this) {
		case TWO:
			return "2";
		case THREE:
			return "3";
		case FOUR:
			return "4";
		case FIVE:
			return "5";
		case SIX:
			return "6";
		case SEVEN:
			return "7";
		case EIGHT:
			return "8";
		case NINE:
			return "9";
		case TEN:
			return "10";
		case JACK:
			return "Jack";
		case QUEEN:
			return "Queen";
		case KING:
			return "King";
		case ACE:
			return "Ace";
		default:
			return this.toString();
		}
	}

}