/* 
 *  Developed By	 : Sreenath T V
 *  Student ID		 : 646823
 *  File Description : Execution of Poker program starts from the main function defined in this file. 
 */

import java.util.Arrays;

public class Poker {

	// To display error messages
	public static void display_error(int num, String temp) {
		if (num == 1) {
			System.out.println("Error: invalid card name '" + temp + "'");
		} else {
			System.out.println("Error: wrong number of arguments; must be a "
					+ "multiple of 5");
		}
	}

	// To declare/display the winner on screen
	public static void displayWinner(player[] winner, int winnerIndex) {
		int[] playerNumberArray = new int[winnerIndex + 1];
		if (winnerIndex == 0) {
			System.out.println("Player "
					+ winner[winnerIndex].getPlayerNumber() + " wins.");
		} else {
			for (int i = 0; i <= winnerIndex; i++) {
				playerNumberArray[i] = winner[i].getPlayerNumber();
			}
			Arrays.sort(playerNumberArray);
			System.out.print("Players ");
			for (int i = 0; i <= winnerIndex - 2; i++) {
				System.out.print(playerNumberArray[i] + ", ");
			}
			System.out.print(playerNumberArray[winnerIndex - 1] + " and "
					+ playerNumberArray[winnerIndex] + " draw");
		}
	}

	/*
	 * This main method parses the command line arguments and creates card
	 * object Also, the player's are allocated cards and calls the classifyHand
	 * method for each player and declare the winner.
	 */

	public static void main(String[] args) {
		int playerNumber = 0, cardNumber = 0;

		cardSuits validSuit = null;
		cardRanks validRank = null;

		if (args.length == 0 || args.length % 5 != 0) {
			display_error(2, "");
			return;
		}

		player[] player = new player[args.length / 5];
		card[] cards = new card[args.length];
		player[playerNumber] = new player(playerNumber);

		for (String countInput : args) {
			validRank = null;
			validSuit = null;

			// Parsing the inputed card ranks
			validRank = cardRanks.parseRanks(countInput.substring(0, 1));

			if (validRank == null) {
				display_error(1, countInput);
				return;
			}

			// Parsing the inputed card suits
			validSuit = cardSuits.parseSuits(countInput.substring(1, 2));

			if (validSuit == null) {
				display_error(1, countInput);
				return;
			}

			/*
			 * Increment the player number after every five cards since one
			 * player has only 5 cards
			 */
			if (cardNumber != 0 && cardNumber % 5 == 0) {
				playerNumber++;
				player[playerNumber] = new player(playerNumber);
			}

			cards[cardNumber] = new card(validRank, validSuit);
			player[playerNumber].addCard(cards[cardNumber], cardNumber % 5);
			cardNumber++;

		}

		for (int i = 0; i < player.length; i++) {
			player[i].classfyHand();
			player[i].display();
		}

		if (player.length > 1) {
			declareWinner(player);
		}
	}

	// Decide the winner and display on screen using the displayWinner method
	public static void declareWinner(player[] playerArray) {

		// Sorting players on the basis of their hand classification
		Arrays.sort(playerArray);

		int arrayLength = playerArray.length;
		int playerNumber = arrayLength - 1;
		int winnerIndex = 0;

		player[] winner = new player[arrayLength];
		player tempWinner;
		winner[winnerIndex] = new player(playerArray[playerNumber]);

		for (int i = playerNumber; i > 0; i--) {
			if (playerArray[i].equals(playerArray[i - 1])) {
				tempWinner = winner[winnerIndex]
						.decideWinner(playerArray[i - 1]);
				if (tempWinner == null) {
					winner[++winnerIndex] = new player(playerArray[i - 1]);
				} else if (tempWinner == playerArray[i - 1]) {
					winner = new player[arrayLength];
					winnerIndex = 0;
					winner[winnerIndex] = new player(playerArray[i - 1]);
				}
			} else {
				break;
			}
		}
		displayWinner(winner, winnerIndex);
	}
}